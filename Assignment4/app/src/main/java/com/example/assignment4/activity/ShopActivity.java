package com.example.assignment4.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assignment4.R;
import com.example.assignment4.adapters.ReflectiveStripAdapter;
import com.example.assignment4.interfaces.ShopMethods;
import com.example.assignment4.model.ReflectiveStrip;
import com.example.assignment4.model.Root;
import com.example.assignment4.services.MyService;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopActivity extends AppCompatActivity {

    private ReflectiveStripAdapter reflectiveStripAdapter;
    private RecyclerView recyclerView;

    //RealmConfiguration configuration;
    Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        /*configuration = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();
        realm = Realm.getInstance(configuration);*/

        ShopMethods methods = MyService.RetrofitInstance.getRetrofitInstance().create(ShopMethods.class);

        Call<Root> call = methods.getListData();

        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<Root>() {
            @Override
            public void onResponse(Call<Root> call, Response<Root> response) {
                generateReflectiveStripList(response.body().products.reflectiveStrip);


                //SAVE IN LOCAL DB
                for (int i = 0; i < response.body().products.reflectiveStrip.size(); i++){
                    realm.beginTransaction();
                    ReflectiveStrip refStrip = realm.createObject(ReflectiveStrip.class);

                    refStrip.title = response.body().products.reflectiveStrip.get(i).title;
                    refStrip.price = response.body().products.reflectiveStrip.get(i).price;
                    refStrip.description = response.body().products.reflectiveStrip.get(i).description;
                    refStrip.discounted_price = response.body().products.reflectiveStrip.get(i).discounted_price;
                    refStrip.shipping_info = response.body().products.reflectiveStrip.get(i).shipping_info;
                    refStrip.image_url = response.body().products.reflectiveStrip.get(i).image_url;
                    refStrip.discounted_price = response.body().products.reflectiveStrip.get(i).discounted_price;
                    refStrip.product_id = response.body().products.reflectiveStrip.get(i).product_id;
                    realm.commitTransaction();
                }
            }

            @Override
            public void onFailure(Call<Root> call, Throwable t) {
                Toast.makeText(ShopActivity.this, "Something went wrong... Error message:" + t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

        RealmResults<ReflectiveStrip> reflectiveStrips = realm.where(ReflectiveStrip.class).findAll();

        ReflectiveStripAdapter adapter = new ReflectiveStripAdapter(reflectiveStrips, ShopActivity.this);
        recyclerView.setAdapter(adapter);
        generateReflectiveStripList(reflectiveStrips);
    }

    private void generateReflectiveStripList(List<ReflectiveStrip> reflectiveStrips){
        recyclerView = findViewById(R.id.product_recyclerview);
        reflectiveStripAdapter = new ReflectiveStripAdapter(reflectiveStrips, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager (layoutManager);
        recyclerView.setAdapter(reflectiveStripAdapter);
    }


}