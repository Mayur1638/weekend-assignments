package com.example.assignment4.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ReflectiveStrip extends RealmObject {

    @PrimaryKey
    @SerializedName("product_id")
    public String product_id;
    @SerializedName("checkout_url")
    public String checkout_url;
    @SerializedName("title")
    public String title;
    @SerializedName("price")
    public String price;
    @SerializedName("description")
    public String description;
    @SerializedName("shipping_info")
    public String shipping_info;
    @SerializedName("discounted_price")
    public String discounted_price;
    @SerializedName("image_url")
    public String image_url;
    @SerializedName("button_text")
    public String button_text;
    @SerializedName("out_of_stock")
    public boolean out_of_stock;

    public String getProduct_id() {
        return product_id;
    }

    public String getCheckout_url() {
        return checkout_url;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getShipping_info() {
        return shipping_info;
    }

    public String getDiscounted_price() {
        return discounted_price;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getButton_text() {
        return button_text;
    }

    public boolean isOut_of_stock() {
        return out_of_stock;
    }

}
