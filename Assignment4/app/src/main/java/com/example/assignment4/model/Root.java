package com.example.assignment4.model;

import com.google.gson.annotations.SerializedName;

public class Root {

    @SerializedName("products")
    public Products products;

}
