package com.example.assignment4.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Products {

    @SerializedName("reflective-strip")
    public List<ReflectiveStrip> reflectiveStrip;


    public List<ReflectiveStrip> getReflectiveStrip() {
        return reflectiveStrip;
    }

    public void setReflectiveStrip(List<ReflectiveStrip> reflectiveStrip) {
        this.reflectiveStrip = reflectiveStrip;
    }
}
