package com.example.assignment4.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class MyService extends Service {

    private final IBinder myBinder = new MyLocalBinder();

    public class MyLocalBinder extends Binder {
        MyService getService(){
            return MyService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }

    public static class RetrofitInstance {

        private static Retrofit retrofit;
        public static final String BASE_URL = "https://inito.com/products/list";

        public static Retrofit getRetrofitInstance(){
            if (retrofit == null){
                retrofit = new retrofit2.Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }
    }

    @Override
    public void sendBroadcast(Intent intent) {
        super.sendBroadcast(intent);
        intent.setAction("com.example.assignment4.services");
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }
}
