package com.example.assignment4.interfaces;

import com.example.assignment4.model.Root;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ShopMethods {

    @GET("")
    Call<Root> getListData();
}
