package com.example.week4assignment.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.week4assignment.R;

public class Profile extends Fragment {
    TextView userName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.show_profile, container, false);

        userName = view.findViewById(R.id.UserName);

        Bundle bundle0 = getArguments();
        if (bundle0!= null){
            String textreceiver = bundle0.getString("text");
            if (textreceiver!=null){
                userName.setText(textreceiver);
            }
        }
        return view;
    }

}
