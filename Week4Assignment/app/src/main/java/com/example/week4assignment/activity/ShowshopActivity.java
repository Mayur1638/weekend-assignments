package com.example.week4assignment.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.week4assignment.R;
import com.example.week4assignment.adapters.ReflectiveStripAdapter;
import com.example.week4assignment.interfaces.ShopMethods;
import com.example.week4assignment.model.ReflectiveStrip;
import com.example.week4assignment.model.Root;
import com.example.week4assignment.network.RetrofitInstance;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowshopActivity extends AppCompatActivity {

    private ReflectiveStripAdapter reflectiveStripAdapter;
    private RecyclerView recyclerView;
    private ReflectiveStrip strip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showshop);

        ShopMethods methods = RetrofitInstance.getRetrofitInstance().create(ShopMethods.class);

        Call<Root> call = methods.getListData();

        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<Root>() {
            @Override
            public void onResponse(Call<Root> call, Response<Root> response) {
                generateReflectiveStripList(response.body().products.reflectiveStrip);
            }

            @Override
            public void onFailure(Call<Root> call, Throwable t) {
                Toast.makeText(ShowshopActivity.this, "Something went wrong... Error message:" + t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

        Fragment shop = new Fragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("key",(Serializable)strip);
        shop.setArguments(bundle);
    }

    private void generateReflectiveStripList(List<ReflectiveStrip> reflectiveStrips){
        recyclerView = findViewById(R.id.product_recyclerView);
        reflectiveStripAdapter = new ReflectiveStripAdapter(reflectiveStrips, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager (layoutManager);
        recyclerView.setAdapter(reflectiveStripAdapter);
    }


}