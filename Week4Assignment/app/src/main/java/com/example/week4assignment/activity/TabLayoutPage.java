package com.example.week4assignment.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.week4assignment.R;
import com.example.week4assignment.adapters.PagerAdapter;
import com.example.week4assignment.interfaces.ShopMethods;
import com.example.week4assignment.model.ReflectiveStrip;
import com.example.week4assignment.model.Root;
import com.example.week4assignment.network.RetrofitInstance;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabLayoutPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);

        //For UserProfile
        SharedPreferences preferences = getSharedPreferences("MAYUR", MODE_PRIVATE);

        String display = preferences.getString("display", "");
        TextView displayInfo = findViewById(R.id.profile_view);
        displayInfo.setText(display);

        //For ShopFragment
        ShopMethods methods = RetrofitInstance.getRetrofitInstance().create(ShopMethods.class);
        Call<Root> call = methods.getListData();

        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<Root>() {
            @Override
            public void onResponse(Call<Root> call, Response<Root> response) {
                generateReflectiveStripList(response.body().products.reflectiveStrip);
            }

            @Override
            public void onFailure(Call<Root> call, Throwable t) {
                Toast.makeText(TabLayoutPage.this, "Something went wrong... Error message:" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void generateReflectiveStripList(List<ReflectiveStrip> reflectiveStrips) {
        ArrayList<ReflectiveStrip> list = (ArrayList<ReflectiveStrip>)reflectiveStrips;
        PagerAdapter sectionsPagerAdapter = new PagerAdapter(getSupportFragmentManager(), this, list);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tab_layout);
        tabs.setupWithViewPager(viewPager);
    }
}