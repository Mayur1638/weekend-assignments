package com.example.week4assignment.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Products {
    @SerializedName("monitor")
    public List<Monitor> monitor;
    @SerializedName("transmissive-strip")
    public List<TransmissiveStrip> transmissiveStrip;
    @SerializedName("reflective-strip")
    public List<ReflectiveStrip> reflectiveStrip;
    @SerializedName("clip")
    public List<Object> clip;

    public List<Monitor> getMonitor() {
        return monitor;
    }

    public void setMonitor(List<Monitor> monitor) {
        this.monitor = monitor;
    }

    public List<TransmissiveStrip> getTransmissiveStrip() {
        return transmissiveStrip;
    }

    public void setTransmissiveStrip(List<TransmissiveStrip> transmissiveStrip) {
        this.transmissiveStrip = transmissiveStrip;
    }

    public List<ReflectiveStrip> getReflectiveStrip() {
        return reflectiveStrip;
    }

    public void setReflectiveStrip(List<ReflectiveStrip> reflectiveStrip) {
        this.reflectiveStrip = reflectiveStrip;
    }

    public List<Object> getClip() {
        return clip;
    }

    public void setClip(List<Object> clip) {
        this.clip = clip;
    }
}
