package com.example.week4assignment.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TransmissiveStripList implements Serializable {
    @SerializedName("transmissive-strip")
    public List<TransmissiveStrip> transmissiveStrip;

    public List<TransmissiveStrip> getTransmissiveStrip() {
        return transmissiveStrip;
    }

    public void setTransmissiveStrip(List<TransmissiveStrip> transmissiveStrip) {
        this.transmissiveStrip = transmissiveStrip;
    }
}
