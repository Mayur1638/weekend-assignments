package com.example.week4assignment.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.week4assignment.R;
import com.example.week4assignment.model.ReflectiveStrip;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReflectiveStripAdapter extends RecyclerView.Adapter<ReflectiveStripAdapter.ReflectiveViewHolder> {

    private List<ReflectiveStrip> reflectiveStrips;
    private Context context;

    public ReflectiveStripAdapter(List<ReflectiveStrip> reflectiveStrips, Context context) {
        this.reflectiveStrips = reflectiveStrips;
        this.context = context;
    }

    @Override
    public ReflectiveViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View rowView = layoutInflater.inflate(R.layout.single_row_view, parent, false);
        return new ReflectiveViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final ReflectiveViewHolder holder, final int position) {
        holder.title.setText(reflectiveStrips.get(position).getTitle());
        Picasso.with(context).load(reflectiveStrips.get(position).getImage_url()).into(holder.product_image);
        holder.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(reflectiveStrips.get(position).getCheckout_url()));
                context.startActivity(i);
            }
        });
        holder.price.setText(reflectiveStrips.get(position).getPrice());
        holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.description.setText(reflectiveStrips.get(position).getDescription());
        holder.shipping_info.setText(reflectiveStrips.get(position).getShipping_info());
        holder.discounted_price.setText(reflectiveStrips.get(position).getDiscounted_price());

    }

    @Override
    public int getItemCount() {
        return reflectiveStrips.size();
    }

    class ReflectiveViewHolder extends RecyclerView.ViewHolder {
        TextView title, price, description, shipping_info, discounted_price;
        ImageView product_image;
        Button addButton;


        public ReflectiveViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.product_id);
            price = itemView.findViewById(R.id.old_price);
            description = itemView.findViewById(R.id.product_des);
            discounted_price = itemView.findViewById(R.id.product_cost);
            shipping_info = itemView.findViewById(R.id.shipping_details);
            product_image = itemView.findViewById(R.id.product_image);
            addButton = itemView.findViewById(R.id.add_button);
        }
    }

}
