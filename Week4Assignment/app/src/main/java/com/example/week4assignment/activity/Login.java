package com.example.week4assignment.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.week4assignment.R;

public class Login extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText userName = findViewById(R.id.etName);
        final EditText password = findViewById(R.id.etPassword);
        Button login = findViewById(R.id.Login);
        Button register = findViewById(R.id.NewRegister);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = userName.getText().toString();
                String passwd = password.getText().toString();

                SharedPreferences preferences = getSharedPreferences("MAYUR", MODE_PRIVATE);
                String userDetails = preferences.getString(user + passwd + "data", "Username or Password is Incorrect!");
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("display", userDetails);
                editor.commit();

                Intent displayScreen = new Intent(Login.this, TabLayoutPage.class);

                startActivity(displayScreen);

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerScreen = new Intent(Login.this, SignUp.class);

                startActivity(registerScreen);
            }
        });

    }
}