package com.example.week4assignment.adapters;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.week4assignment.Fragment.Camera;
import com.example.week4assignment.Fragment.Profile;
import com.example.week4assignment.Fragment.Shop;
import com.example.week4assignment.R;
import com.example.week4assignment.model.ReflectiveStrip;

import java.util.ArrayList;

public class PagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3};
    private final Context mContext;
    ArrayList<ReflectiveStrip> refStrip;
    String profileDetails;

    public PagerAdapter(FragmentManager fm, Context mContext, ArrayList<ReflectiveStrip> refStrip) {
        super(fm);
        this.mContext = mContext;
        this.refStrip = refStrip;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new Profile();
                Bundle bundle0 = new Bundle();
                bundle0.putString("text", profileDetails);
                fragment.setArguments(bundle0);
                return fragment;

            case 1:
                fragment = new Shop();
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("key", refStrip);
                fragment.setArguments(bundle1);
                return fragment;

            case 2:
                fragment = new Camera();
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
