package com.example.week4assignment.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.week4assignment.R;
import com.example.week4assignment.adapters.ReflectiveStripAdapter;
import com.example.week4assignment.model.ReflectiveStrip;

import java.util.List;

public class Shop extends Fragment {

    private ReflectiveStripAdapter reflectiveStripAdapter;
    private RecyclerView customView;
    private List<ReflectiveStrip> refStrip;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle1 = getArguments();
        refStrip = (List<ReflectiveStrip>) bundle1.getSerializable("key");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.show_shop, container, false);
        customView = view.findViewById(R.id.product_list);
        generateReflectiveStripList();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void generateReflectiveStripList(){
        customView = customView.findViewById(R.id.product_list);
        reflectiveStripAdapter = new ReflectiveStripAdapter(refStrip, getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        customView.setLayoutManager (layoutManager);
        customView.setAdapter(reflectiveStripAdapter);
    }
}
