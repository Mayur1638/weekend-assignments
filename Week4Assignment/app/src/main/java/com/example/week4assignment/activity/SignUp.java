package com.example.week4assignment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.week4assignment.R;

public class SignUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        final EditText userName = findViewById(R.id.etName);
        final EditText signUpID = findViewById(R.id.etEmailID);
        final EditText password = findViewById(R.id.etPassword);

        Button registerButton = findViewById(R.id.Registerbtn);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getSharedPreferences("MAYUR", MODE_PRIVATE);
                String newUser = userName.getText().toString();
                String newEmail = signUpID.getText().toString();
                String newPass = password.getText().toString();

                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(newUser+newPass +"data", newUser +"\n" + newEmail);
                editor.commit();

                Intent loginScreen = new Intent(SignUp.this, Login.class);
                startActivity(loginScreen);
            }
        });
    }
}