package com.example.week4assignment.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MonitorList implements Serializable {
    @SerializedName("monitor")
    public List<Monitor> monitor;

    public List<Monitor> getMonitor() {
        return monitor;
    }

    public void setMonitor(List<Monitor> monitor) {
        this.monitor = monitor;
    }
}
