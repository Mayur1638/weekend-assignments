package com.example.week4assignment.model;

import com.google.gson.annotations.SerializedName;

public class Information{

    @SerializedName("shop_message")
    public String shopMessage;

    public String getShopMessage() {
        return shopMessage;
    }

    public void setShopMessage(String shopMessage) {
        this.shopMessage = shopMessage;
    }
}
