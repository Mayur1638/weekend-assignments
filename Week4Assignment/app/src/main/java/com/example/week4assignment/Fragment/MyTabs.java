package com.example.week4assignment.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.week4assignment.R;

public class MyTabs extends Fragment {

    public MyTabs() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.my_tabs, container, false);

        TextView textView = view.findViewById(R.id.text_view);
        String sTitle = getArguments().getString("title");
        textView.setText(sTitle);
        return view;
    }
}