package com.example.week4assignment.interfaces;

import com.example.week4assignment.model.Root;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ShopMethods {
    @GET("products/list")
    Call<Root> getListData();
}
