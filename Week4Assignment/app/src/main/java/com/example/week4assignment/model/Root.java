package com.example.week4assignment.model;

import com.google.gson.annotations.SerializedName;

public class Root {
    @SerializedName("products")
    public Products products;

    @SerializedName("information")
    public Information information;
}
